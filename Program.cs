﻿using System;

namespace exercise_13
{
    class Program
    {
        static void Main(string[] args)
        {
          //Start the program with Clear();
          Console.Clear();
          
          var myname = "justin";

          Console.WriteLine($"Hello my name is { myname }:");
          
          //End the program with blank line and instructions
          Console.ResetColor();
          Console.WriteLine();
          Console.WriteLine("Press <Enter> to quit the program");
          Console.ReadKey();


        }
    }
}
